package com.atlassian.stash.plugin.automerge.internal;

import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.google.common.base.Strings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.net.URI;
import java.util.*;

public abstract class XsrfProtectedServlet extends HttpServlet {

    protected final SoyTemplateRenderer soyTemplateRenderer;
    protected final XsrfTokenAccessor xsrfTokenAccessor;
    protected final XsrfTokenValidator xsrfTokenValidator;
    protected final NavBuilder navBuilder;
    protected final StashAuthenticationContext authContext;
    protected final AuthenticationController authenticationController;
    protected final LoginUriProvider loginUriProvider;

    public XsrfProtectedServlet(
            SoyTemplateRenderer soyTemplateRenderer,
            XsrfTokenAccessor xsrfTokenAccessor,
            XsrfTokenValidator xsrfTokenValidator,
            NavBuilder navBuilder,
            StashAuthenticationContext authContext,
            AuthenticationController authenticationController,
            LoginUriProvider loginUriProvider) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.xsrfTokenAccessor = xsrfTokenAccessor;
        this.xsrfTokenValidator = xsrfTokenValidator;
        this.navBuilder = navBuilder;
        this.authContext = authContext;
        this.authenticationController = authenticationController;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (authenticationController.shouldAttemptAuthentication(req)) {
            sendRedirectToLogin(req, resp);
            return;
        }
        doGetInternal(req, resp);
    }

    protected abstract void doGetInternal(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (authenticationController.shouldAttemptAuthentication(req)) {
            sendRedirectToLogin(req, resp);
            return;
        }
        if (!xsrfTokenValidator.validateFormEncodedToken(req)) {
            req.getRequestDispatcher(navBuilder.xsrfNotification().buildRelNoContext()).forward(req, resp);
            return;
        }

        doXsrfProtectedPost(req, resp);
    }

    protected abstract void doXsrfProtectedPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

    protected void sendRedirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestUri = req.getRequestURI();
        String contextPath = req.getContextPath();
        if (!Strings.isNullOrEmpty(contextPath)) {
            requestUri = requestUri.substring(contextPath.length());
        }
        resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(requestUri)).toString());
    }

    protected void renderToResponse(HttpServletRequest req, HttpServletResponse resp, String completeModuleKey, String templateName, Map<String, Object> model) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), completeModuleKey, templateName, model, getInjectedData(req, resp));
        } catch (SoyException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new ServletException(e);
            }
        }
    }

    protected Map<String, Object> getInjectedData(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, Object> injectedData = new HashMap<String, Object>();
        injectedData.put("xsrfTokenName", xsrfTokenValidator.getXsrfParameterName());
        injectedData.put("xsrfTokenValue", xsrfTokenAccessor.getXsrfToken(req, resp, true));
        injectedData.put("principal", authContext.getCurrentUser());
        return injectedData;
    }

    protected Map<String, List<String>> toFieldErrors(Set<ConstraintViolation<?>> constraintViolations) {
        if (constraintViolations == null) {
            return null;
        }
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        for (ConstraintViolation constraintViolation : constraintViolations) {
            String key = constraintViolation.getPropertyPath().toString();
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<String>());
            }
            map.get(key).add(constraintViolation.getMessage());
        }
        return map;
    }

}
