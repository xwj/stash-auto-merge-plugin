package com.atlassian.stash.plugin.automerge.internal;

import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.plugin.automerge.AutoMergeSettingsService;
import com.atlassian.stash.repository.Branch;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.google.common.base.Strings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutoMergeSettingsServlet extends XsrfProtectedServlet {

    private final RepositoryService repositoryService;
    private final AutoMergeSettingsService autoMergeSettingsService;
    private final PermissionValidationService permissionValidationService;

    public AutoMergeSettingsServlet(SoyTemplateRenderer soyTemplateRenderer, XsrfTokenAccessor xsrfTokenAccessor, XsrfTokenValidator xsrfTokenValidator, NavBuilder navBuilder, StashAuthenticationContext authContext, AuthenticationController authenticationController, LoginUriProvider loginUriProvider, RepositoryService repositoryService, AutoMergeSettingsService autoMergeSettingsService, PermissionValidationService permissionValidationService) {
        super(soyTemplateRenderer, xsrfTokenAccessor, xsrfTokenValidator, navBuilder, authContext, authenticationController, loginUriProvider);
        this.repositoryService = repositoryService;
        this.autoMergeSettingsService = autoMergeSettingsService;
        this.permissionValidationService = permissionValidationService;
    }

    private Repository getRepository(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        String[] pathParts = pathInfo.substring(1).split("/");
        if (pathParts.length != 2) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        String projectKey = pathParts[0];
        String repoSlug = pathParts[1];
        Repository repository = repositoryService.findBySlug(projectKey, repoSlug);
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        return repository;
    }

    @Override
    protected void doGetInternal(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Repository repository = getRepository(req, resp);
        if (repository == null) {
            return;
        }
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);

        String branches = autoMergeSettingsService.getAutoMergeSettings(repository);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("repository", repository);
        model.put("branches", branches);
        renderToResponse(req, resp,
                "com.atlassian.stash.plugin.auto-merge-plugin:soy-templates",
                "stash.plugin.autoMergeForm",
                model);
    }

    private Object toBranchString(Iterable<Branch> branches) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (Branch branch : branches) {
            sb.append(sep).append(branch.getDisplayId());
            sep = " -> ";
        }
        return sb.toString();
    }

    @Override
    protected void doXsrfProtectedPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Repository repository = getRepository(req, resp);
        if (repository == null) {
            return;
        }

        String branchesAsString = Strings.nullToEmpty(req.getParameter("branches"));
        autoMergeSettingsService.setAutoMergeSettings(repository, branchesAsString);

        resp.sendRedirect(req.getRequestURI());
    }

}
