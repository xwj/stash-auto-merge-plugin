package com.atlassian.stash.plugin.automerge;

import com.atlassian.stash.repository.Branch;
import com.atlassian.stash.repository.Repository;

import java.util.List;

public interface AutoMergeSettingsService {

    List<List<Branch>> getAutoMergeBranches(Repository repository);
    String getAutoMergeSettings(Repository repository);

    void setAutoMergeSettings(Repository repository, String branchNames);
}
