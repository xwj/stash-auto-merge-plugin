package com.atlassian.stash.plugin.automerge.internal;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.event.RepositoryPushEvent;
import com.atlassian.stash.event.RepositoryRefsChangedEvent;
import com.atlassian.stash.event.pull.PullRequestMergedEvent;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.plugin.automerge.AutoMergeSettingsService;
import com.atlassian.stash.pull.*;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.scm.MergeCommandParameters;
import com.atlassian.stash.scm.ScmService;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.util.*;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;

public class PullRequestMergeProcessor {

    private static final Logger log = LoggerFactory.getLogger(PullRequestMergeProcessor.class);

    private static final PageRequest SINGLE_ITEM = new PageRequestImpl(0, 1);

    private static final Predicate<RefChange> UPDATES = new Predicate<RefChange>() {
        public boolean apply(RefChange refChange) {
            return refChange.getType() != RefChangeType.DELETE;
        }
    };

    private static final Function<RefChange, String> PLUCK_REF_ID = new Function<RefChange, String>() {
        public String apply(RefChange refChange) {
            return refChange.getRefId();
        }
    };

    private final AutoMergeSettingsService autoMergeSettingsService;
    private final SecurityService securityService;
    private final HistoryService historyService;
    private final ScmService scmService;
    private final StashAuthenticationContext authContext;
    private final PullRequestService pullRequestService;
    private final EventPublisher eventPublisher;
    private final MailService mailService;

    public PullRequestMergeProcessor(
            AutoMergeSettingsService autoMergeSettingsService,
            SecurityService securityService,
            HistoryService historyService,
            ScmService scmService,
            StashAuthenticationContext authContext, PullRequestService pullRequestService, EventPublisher eventPublisher, MailService mailService) {
        this.autoMergeSettingsService = autoMergeSettingsService;
        this.securityService = securityService;
        this.historyService = historyService;
        this.scmService = scmService;
        this.authContext = authContext;
        this.pullRequestService = pullRequestService;
        this.eventPublisher = eventPublisher;
        this.mailService = mailService;
    }

    @EventListener
    public void onPullRequestMerged(RepositoryRefsChangedEvent event) {
        final Repository repository = event.getRepository();
        final Set<String> modifiedBranchIds = Sets.newHashSet(Collections2.transform(Collections2.filter(event.getRefChanges(), UPDATES), PLUCK_REF_ID));
        List<List<Branch>> configuredBranches = autoMergeSettingsService.getAutoMergeBranches(repository);
        List<Branch> sourceBranches = configuredBranches.get(0);
        List<Branch> targetBranches = configuredBranches.get(1);
        Branch branch = Iterables.find(sourceBranches, new Predicate<Branch>() {
            public boolean apply(Branch branch) {
                return modifiedBranchIds.contains(branch.getId());
            }
        }, null);
        if (branch != null) {
            final List<Branch> autoMergeSourceBranches = new ArrayList<Branch>();
            final List<Branch> autoMergeTargetBranches = new ArrayList<Branch>();
            for(int i = 0; i < sourceBranches.size();i++){
                if(sourceBranches.get(i).getDisplayId() == branch.getDisplayId()){
                    autoMergeSourceBranches.add(sourceBranches.get(i));
                    autoMergeTargetBranches.add(targetBranches.get(i));
                }
            }
            if (autoMergeSourceBranches.size() > 1) {
                log.info("Auto merging branches for {}/{}", repository.getProject().getKey(), repository.getSlug());
                securityService.doAsUser("Auto merging branches", event.getUser().getName(), new Operation<Void, RuntimeException>() {
                    public Void perform() {
                        autoMerge(repository, autoMergeSourceBranches,autoMergeTargetBranches);
                        return null;
                    }
                });
            }
        }
    }

    private void autoMerge(Repository repository, List<Branch> autoMergeSourceBranches,List<Branch> autoMergeTargetBranches) {
        for(int i = 0; i < autoMergeSourceBranches.size();i++){
            if(openPullRequestExists(repository,autoMergeSourceBranches.get(i),autoMergeTargetBranches.get(i))){
                log.info("Not auto merging {} to {} as there is an open pull request for these branches", autoMergeSourceBranches.get(i).getDisplayId(), autoMergeTargetBranches.get(i).getDisplayId());
                continue;
            }
            Page<Changeset> changes = historyService.getChangesetsBetween(repository,autoMergeTargetBranches.get(i).getId(), autoMergeSourceBranches.get(i).getId(),SINGLE_ITEM);
            if(changes.getSize() > 0){
            try{
                log.info("Attempting to auto merging {} to {}", autoMergeSourceBranches.get(i).getDisplayId(), autoMergeTargetBranches.get(i).getDisplayId());
                autoMerge(repository, autoMergeSourceBranches.get(i), autoMergeTargetBranches.get(i));
            }catch(Exception e){
                log.info("Failed to auto merging {} to {}. Creating a pull request instead", autoMergeSourceBranches.get(i).getDisplayId(), autoMergeTargetBranches.get(i).getDisplayId());
                sendErrorEmail(repository, autoMergeSourceBranches.get(i), autoMergeTargetBranches.get(i), e.getLocalizedMessage());
                }
            }
        }
    }

    private boolean openPullRequestExists(final Repository repository, final Branch sourceBranch, final Branch destinationBranch) {
        // Dont even ask
        return intersection(
                new PagedIterable<PullRequest>(
                        new PageProvider<PullRequest>() {
                            @Override
                            public Page<PullRequest> get(PageRequest pageRequest) {
                                return pullRequestService.findInDirection(
                                        PullRequestDirection.INCOMING,
                                        repository.getId(), destinationBranch.getId(),
                                        PullRequestState.OPEN, PullRequestOrder.OLDEST,
                                        pageRequest
                                );
                            }
                        },
                        new PageRequestImpl(0, 25)
                ).iterator(),
                new PagedIterable<PullRequest>(
                        new PageProvider<PullRequest>() {
                            @Override
                            public Page<PullRequest> get(PageRequest pageRequest) {
                                return pullRequestService.findInDirection(
                                        PullRequestDirection.OUTGOING,
                                        repository.getId(), sourceBranch.getId(),
                                        PullRequestState.OPEN, PullRequestOrder.OLDEST,
                                        pageRequest
                                );
                            }
                        },
                        new PageRequestImpl(0, 25)
                ).iterator(),
                new Comparator<PullRequest>() {
                    @Override
                    public int compare(PullRequest a, PullRequest b) {
                        if (a.getId().equals(b.getId())) {
                            return 0;
                        }
                        return a.getCreatedDate().compareTo(b.getCreatedDate());
                    }
                }
        ).hasNext();
    }

    private <T> Iterator<T> intersection(final Iterator<T> a, final Iterator<T> b, final Comparator<T> comparator) {
        return new Iterator<T>() {
            private T next;
            @Override
            public boolean hasNext() {
                return next != null || (a.hasNext() && b.hasNext() && peek() != null);
            }

            private T peek() {
                if (next != null) {
                    return next;
                }
                T currentA = null;
                T currentB = null;
                while (a.hasNext() && b.hasNext()) {
                    if (currentA == null) {
                        currentA = a.next();
                    }
                    if (currentB == null) {
                        currentB = b.next();
                    }
                    int compare = comparator.compare(currentA, currentB);
                    if (compare < 0) {
                        currentA = null;
                    } else if (compare > 0) {
                        currentB = null;
                    } else {
                        next = currentA;
                        break;
                    }
                }
                return next;
            }

            @Override
            public T next() {
                T next = peek();
                if (next == null) {
                    throw new NoSuchElementException();
                }
                this.next = null;
                return next;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    private void createPullRequest(Repository repository, Branch sourceBranch, Branch destinationBranch, String localizedMessage) {
        try {
            pullRequestService.create(
                    String.format("Cascading merge from %s to %s", sourceBranch.getDisplayId(), destinationBranch.getDisplayId()),
                    "Raising a pull request because the automatic merge failed: " + localizedMessage,
                    Collections.<String>emptySet(),
                    repository,
                    sourceBranch.getId(),
                    repository,
                    destinationBranch.getId()
            );
        } catch (Exception e) {
            if (!(e instanceof DuplicatePullRequestException)) {
                // TODO log warning
            }
        }
    }

    private void sendErrorEmail(Repository repository, Branch sourceBranch, Branch destinationBranch, String localizedMessage) {
        mailService.submit(new MailMessage.Builder()
                .to(authContext.getCurrentUser().getEmailAddress())
                .subject(String.format("Automatic merge failed for %s / %s. Please merge manually", repository.getProject().getName(), repository.getName()))
                .text(String.format("The automatic merge from %s to %s failed. Please merge the changes manually.\n\nCause: %s", sourceBranch.getDisplayId(), destinationBranch.getDisplayId(), localizedMessage))
                .build());
    }

    private void autoMerge(Repository repository, Branch sourceBranch, Branch destinationBranch) {
        Branch updatedBranch = scmService.getCommandFactory(repository)
                .merge(new MergeCommandParameters.Builder()
                        .fromBranch(sourceBranch.getId())
                        .toBranch(destinationBranch.getId())
                        .author(authContext.getCurrentUser())
                        .message(String.format("Automatic merge from %s -> %s", sourceBranch.getDisplayId(), destinationBranch.getDisplayId()))
                        .build())
                .call();
        eventPublisher.publish(new RepositoryPushEvent(this, repository, Collections.<RefChange>singletonList(new InternalRefChange(updatedBranch.getId(), destinationBranch.getLatestChangeset(), updatedBranch.getLatestChangeset(), RefChangeType.UPDATE))));
    }

    private static <T> Predicate<T> comparableTo(final T a, final Comparator<T> comparator) {
        return new Predicate<T>() {
            public boolean apply(T b) {
                return comparator.compare(a, b) == 0;
            }
        };
    }

}
