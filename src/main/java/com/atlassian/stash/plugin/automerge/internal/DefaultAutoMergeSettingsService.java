package com.atlassian.stash.plugin.automerge.internal;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.plugin.automerge.AutoMergeSettingsService;
import com.atlassian.stash.repository.Branch;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.BranchesCommandParameters;
import com.atlassian.stash.scm.git.GitAgent;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

public class DefaultAutoMergeSettingsService implements AutoMergeSettingsService {

    private final GitAgent gitAgent;
    private final GitScm gitScm;
    private final PluginSettings pluginSettings;
    private final PermissionValidationService permissionValidationService;

    private static final PageRequest HUNDRED_ITEMS = new PageRequestImpl(0, 99);

    private final ConcurrentMap<Integer, String> cache = new MapMaker()
            .makeComputingMap(new Function<Integer, String>() {
                @Override
                public String apply(Integer repoId) {
                    Object branchNames = pluginSettings.get(Integer.toString(repoId));
                    return branchNames instanceof String ? (String)branchNames : "";
                }
            });

    public DefaultAutoMergeSettingsService(GitAgent gitAgent,GitScm gitScm, PluginSettingsFactory pluginSettingsFactory, PermissionValidationService permissionValidationService) {
        this.gitAgent = gitAgent;
        this.gitScm = gitScm;
        this.permissionValidationService = permissionValidationService;
        pluginSettings = pluginSettingsFactory.createSettingsForKey("com.atlassian.stash.plugin.auto-merge-plugin");
    }

    @Override
    public List<List<Branch>> getAutoMergeBranches(Repository repository) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_READ);
        return resolveBranches(repository, cache.get(repository.getId()));
    }

    @Override
    public String getAutoMergeSettings(Repository repository){
        permissionValidationService.validateForRepository(repository, Permission.REPO_READ);
        return cache.get(repository.getId());
    }

    /*@Override
    public void setAutoMergeBranches(Repository repository, List<List<String>> branchNames) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        pluginSettings.put(Integer.toString(repository.getId()), branchNames);
        cache.remove(repository.getId());
    }*/

    @Override
    public void setAutoMergeSettings(Repository repository, String branchNames){
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        if(!branchNames.isEmpty()){
            pluginSettings.put(Integer.toString(repository.getId()),branchNames);
        }else{
            pluginSettings.remove(Integer.toString(repository.getId()));
        }
        cache.remove(repository.getId());
    }

    private List<List<Branch>> resolveBranches(final Repository repository, String branchConfigurations) {
        if (!GitScm.ID.equals(repository.getScmId())) {
            return Collections.emptyList();
        }
        /*return Lists.newArrayList(Iterables.flatMap(branchOrder, new Function<String, Iterable<Branch>>() {
            public Option<Branch> apply(String branchName) {
                return Option.option(gitAgent.resolveBranch(repository, branchName));
            }
        }));*/
        List<Branch> sourceBranches = new ArrayList<Branch>();
        List<Branch> targetBranches = new ArrayList<Branch>();
        Collection<Branch> allBranches = gitScm.getCommandFactory().branches(repository, new BranchesCommandParameters.Builder().build(), HUNDRED_ITEMS).call().getOrdinalIndexedValues().values();
        Branch[] serverBranches = allBranches.toArray(new Branch[allBranches.size()]);
        String[] branchConfigs = branchConfigurations.split("\\s*;\\s*") ;
        for(String branchConfig: branchConfigs){
            String[] branches = branchConfig.split("\\s*->\\s*");
            String sourceBranchPattern = "^" + branches[0].replace("*",".*") + "$";
            String targetBranchPattern = "^" + branches[1].replace("*",".*") + "$";
            for(Branch sourceBranch:serverBranches){
                for(Branch targetBranch:serverBranches){
                    if(Pattern.matches(sourceBranchPattern,sourceBranch.getDisplayId()) && Pattern.matches(targetBranchPattern,targetBranch.getDisplayId())){
                        sourceBranches.add(sourceBranch);
                        targetBranches.add(targetBranch);
                    }
                }
            }
        }
        List<List<Branch>> returnList = new ArrayList<List<Branch>>();
        returnList.add(sourceBranches);
        returnList.add(targetBranches);
        return returnList;
    }
}
